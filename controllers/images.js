const {Image, User} = require("../models/associations");
const imageSchema = require("../models/imageSchema");

const postImages = async(req, res) => {
    const {body} = req;

    try {
        const existImage = await Image.findOne({
            attributes: ['id' ,'uid', 'mongoId'],
            where:{
                uid: body.uid
            }
        });

        if (existImage) {
            return res.status(400).json({
                msg: `Este usuario ya tiene una imagen registrada`
            })
        }

        const mongo_image = new imageSchema({image: body.image});
        await mongo_image.save();

        const mysql_image = new Image({uid: body.uid, mongoId: mongo_image.id});
        await mysql_image.save();

        res.json({
            message: 'Imagen guardada con exito'
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Comuniquese con el administrador'
        });
    }
}

const putImage = async(req, res) => {
    const {uid} = req.params;
    const {body} = req;

    try {
        const user = await User.findByPk(uid);

        if (!user) {
            return res.status(404).json({
                msg: `No existe un usuario con el id ${id}`
            })
        }

        const mysql_image = await Image.findOne({
            attributes:['id', 'uid', 'mongoId'],
            where: {
                uid: uid
            }
        });

        if (!mysql_image) {
            return res.status(404).json({
                msg: `Este usuario no tiene imagenes para editar, porfavor suba una imagen`
            })
        }

        const mongo_image = await imageSchema.findById(mysql_image.mongoId);

        await mongo_image.updateOne({image: body.image});

        res.json({
            message: 'Imagen Actualizada con exito'
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Comuniquese con el administrador'
        });
    }
}

const deleteImage = async(req,res) => {
    const {uid} = req.params;
    try {
        const user = await User.findByPk(uid);

        if (!user) {
            return res.status(404).json({
                msg: `No existe un usuario con el id ${id}`
            })
        }

        const mysql_image = await Image.findOne({
            attributes:['id', 'uid', 'mongoId'],
            where: {
                uid: uid
            }
        });

        if (!mysql_image) {
            return res.status(404).json({
                msg: `Este usuario no tiene imagenes para eliminar`
            })
        }

        const mongo_image = await imageSchema.findById(mysql_image.mongoId);

        await mysql_image.destroy();
        await mongo_image.remove();

        res.json({
            message: 'Imagen eliminada con exito'
        })
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Comuniquese con el administrador'
        });        
    }
}

module.exports = {
    postImages,
    putImage,
    deleteImage
}