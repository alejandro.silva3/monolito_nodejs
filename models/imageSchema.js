const {Schema, model} = require('mongoose');

const ImageSchema = Schema({
    image: {
        type: String,
        required: true
    }
});

module.exports = model('Image', ImageSchema);