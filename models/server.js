const express = require('express');
const cors = require('cors');

const db = require('../db/mysql/connection.mysql');
const { dbConnection } = require('../db/mongodb/connection.mongo');

class Server {
    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.paths = {
            userPath: '/api/users',
            imagesPath: '/api/images'
        }
        this.dbConnection();
        this.middlewares();
        this.routes();
    }

    async dbConnection() {
        try {
            
            await db.authenticate();
            dbConnection();
            console.log('MySQL online');

        } catch (error) {
            throw new Error(error);
        }
    }

    middlewares() {
        this.app.use( cors() );
        this.app.use( express.json() );
        this.app.use( express.static('public') );
    }

    routes() {
        this.app.use(this.paths.userPath, require('../routes/users'));
        this.app.use(this.paths.imagesPath, require('../routes/images'));
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en el puerto ${this.port}`);
        })
    }
}

module.exports = Server;