const {DataTypes} = require('sequelize');
const db = require('../db/mysql/connection.mysql');

const User = db.define('User', {
    id: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING
    },
    age: {
        type: DataTypes.INTEGER
    },
    email: {
        type: DataTypes.STRING
    },
    state: {
        type: DataTypes.TINYINT,
        defaultValue: 1
    }
}, 
{
    timestamps: false
});

module.exports = User;