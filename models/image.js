const {DataTypes} = require('sequelize');
const db = require('../db/mysql/connection.mysql');

const Image = db.define('Image', {
    uid: {
        type: DataTypes.STRING,
    },
    mongoId: {
        type: DataTypes.STRING
    }
}, 
{
    timestamps: false
});

module.exports = Image;