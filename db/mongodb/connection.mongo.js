const mongoose = require('mongoose');

const dbConnection = () => {
    try {
        
        mongoose.connect(process.env.MONGODB_CONNECTION);
        console.log('Mongo online');

    } catch (error) {
        console.log(error);
        throw new Error('Error al conectar base de datos mongo');
    }
}

module.exports = {
    dbConnection
}