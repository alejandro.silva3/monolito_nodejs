const { Sequelize } = require('sequelize');

//error con env
const db = new Sequelize(process.env.MYSQL_DB , process.env.MYSQL_USER, process.env.MYSQL_PASS, {
    host: process.env.MYSQL_DBHOST,
    dialect: 'mysql',
    logging: false
});



module.exports = db;